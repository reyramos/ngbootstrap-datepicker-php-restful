﻿## About ##
This is a demonstration using Angular JS and Angular Bootstrap date-picker to make RESTful request to a PHP server.

Angular Js 1.2.1 has release a $resource, a factory which creates object to interact with RESTful request.
$resource still uses $http to perform the request, but I have found this could have limitation to url queries string.

I would recommend using $http.put(url,object).success(callback)
$http.put will place http request in the body, which you can called PHP function http_get_request_body();

Unfortunately you will need to have PECL-HTTP module installed, which you you have shared hosting accounts like Godaddy, this is not possible.
Another option is to used file_get_contents('php://input'), which it gives you direct access to the input stream,
as opposed to accessing the data after PHP has already applied the $_GET/$_POST super globals.

##Note##
Update your route to your PHP RESTapi, located within the app/js/calendarDirective.js file
$http.post('/api',obj)

## Resources ##
Angular Bootstrap date-picker <http://angular-ui.github.io/bootstrap/>
ng.$http <http://code.angularjs.org/1.2.1/docs/api/ng.$http#methods_put/>
ngResource.$resource <http://code.angularjs.org/1.2.1/docs/api/ngResource.$resource/>
php://<http://php.net/manual/en/wrappers.php.php>
http_get_request_body <http://php.net/manual/en/function.http-get-request-body.php>




