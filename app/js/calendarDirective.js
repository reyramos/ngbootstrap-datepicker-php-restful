/**
 * CALLBACK FUNCTIONS:
 *
 * getDates = return data of json request
 * gotDates = return json object
 * updateUI = return json data
 * selectedDate = return date String, scope.selectedDate is also public
 * calendarDataUpdated = return json loadedDates
 */

angular.module( 'app' )
	.directive
	( 'calendarDirective',['$http'
		, function($http){
			var dir =
			{ template:
				'<div'
					+ ' datepicker'
					+ ' ng-model="selectedDate"'
					+ ' show-weeks="showWeeks"'
					+ ' starting-day="0"'  //if you want the change the order of the weekdays sun = 0,mon ... saturday = 6
					+ ' date-disabled="disabled( date, mode )"'
					+ ' min="minDate"'
					+ ' max="maxDate">'
					+'</div>'

				, link: function( scope, elm, attrs ){
				/**
				 * search for scope variables
				 * allow for custom scope keys to be set and gathered
				 * @type {*}
				 */

				var   options = scope[ attrs.options ]
					, callbacks= scope[ attrs.callbacks ]


				// Inherits angularjs datepicker directive and ammends functionality below.
				// Fetch new date on month change.
				// Iterate through calendar month and set enable/disable dates.
				// Highlight first available date.

				// If options are not defined, chances are the user refreshed the page on the calendar.
				if( !options ){
					return;
				}

				var  loadedDates
					, firstRun = true
					, fetching = false
					, requestMonth = -1 // Current month based on api RESTful
					, monthsRequested = []
					, months = [ 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december' ];

				// Just a reference to the current date to avoid creating new Date() all the time.
				var todaysDate = new Date()
					, todaysYear = todaysDate.getFullYear()
					, todaysMonth = todaysDate.getMonth()
					, todaysDay = todaysDate.getDate();

				/***-----------------------------------------------------------------------
				 * PUBLIC
				 *------------------------------------------------------------------------**/
				var calData = null;

				// Don't show weeks in the calendar.
				// this is scope configuration in the datepicker library
				scope.showWeeks = options.showWeeks;

				// Set the min and max dates. NOTE: These are based on users date and time setting.
				scope.minDate = new Date();
				scope.maxDate = new Date();

				// set the max month to search by
				scope.maxDate.setMonth( scope.maxDate.getMonth() + 12 );

				// Selected date.
				scope.selectedDate = null;

				/**
				 * START HIJACKED FUNCTIONS ===========================================
				 * This $watch will only apply to hijacke function from datepicker library
				 * @type {function()|*}
				 */
				var getCalData = scope.$watch
				( function(){
					  return scope.$$childHead;
				  }
					, function( childHead ){

					  //We are Hijacked library to modify function
					  if( !childHead ){
						  return;
					  }

					  // Set a local reference to the calendar's data.
					  calData = childHead;
					  // Get a reference to datepicker move function. It will be overridden later.
					  scope._move = calData.move;
					  calData.move = scope.moveFunction;

					  scope._select = calData.select;
					  calData.select = scope.selectFunction;

					  // Remove watch now that we have what we need.
					  getCalData();
				  }
				);
				// Hijacked datePicker move function.
				scope.moveFunction = function( step ){
					if( step == -1 || step == 1 ){
						scope._move( step );
					}

					// Select a date in the middle of the selected month and use that for requesting new dates.
					var midDate = calData.rows[ 1 ][ 6 ].date;
					if( midDate ){
						scope.selectedMonthName = months[ midDate.getMonth() ] + ' ' + midDate.getFullYear();

						if( scope.selectedMonthName && !firstRun ){
							requestDateBySelectedMonth( scope.selectedMonthName );

						}
					}
				}

				// Hijacked datePicker select function.
				scope.selectFunction = function( dt ) {

					scope.selectedDate = dt

					// start of datepicker.js select function
					// the following is no longer needed with the new angular bootstrap date-picker library
					// Notify controller we're making request.
					cb( 'selectedDate', scope.selectedDate );

				};

				/**
				 * END HIJACKED FUNCTIONS ===========================================
				 */


				scope.disabled = function( date, mode ){

					console.log('disabled >> loadedDates', loadedDates)

					// Enables/disables days based on result from the server.
					// Return true = disabled
					// Return false = enabled
					var currentDateString
						, monthNameStr
						, loadedYearObject
						, loadedMonthObject;

					if( mode != 'day' ){
						return true;
					}

					//if there is no loaded dates from the server respond
					if( !loadedDates ){
						return true;
					}

					monthNameStr = months[ date.getMonth() ].toLowerCase();
					loadedYearObject = loadedDates[ 'year' + date.getFullYear() ];

					if( !loadedYearObject ){
						return true;
					}

					loadedMonthObject = loadedYearObject[ months[ date.getMonth() ] ];

					if( !loadedMonthObject ){
						return true;
					}

					// Enable the date if the date exists in loadedDates.
					currentDateString = gateDateStringByDate( date );
					if( loadedYearObject[ monthNameStr ] && loadedYearObject[ monthNameStr ][ currentDateString ] ){
						return false;
					}

					return true;

				}

				/***-----------------------------------------------------------------------
				 * PRIVATE
				 *------------------------------------------------------------------------**/

				function requestDates(){
					// Verify the request was not previously made for this date.
					// If so, load the dates from memory.
					if( fetching ){
						return;
					}

					for( var i = 0; i < monthsRequested.length; i++ ){
						if( monthsRequested[ i ].month == requestMonth && monthsRequested[ i ].served ){
							return;
						}
					}

					// Remember the months requested.
					monthsRequested.push( { month: requestMonth, served: false } );

					var reqObj = {};
					reqObj = options.requestObj;
					reqObj.request_type = options.request_type;
					reqObj.month = requestMonth;
					reqObj.start_date = requestMonth;
					reqObj.end_date = ( requestMonth > -1 ) ? requestMonth + 1 : todaysMonth + 2;


					// Notify controller we're making request.
					cb( 'getDates', reqObj );

					fetching = true;

					//UPDATE TO YOUR API DIRECTORY
					$http.post('http://localhost/ngbootstrap-datepicker-php-restful/api/',reqObj).success(function(data) {
						// Notify controller we've got dates.
						cb( 'gotDates', data );
						// Update the firstRun flag.
						firstRun = false;
						if( data.status == 'OK' ){
							// Great! We have dates. Update UI.
							for( var i = 0; i < monthsRequested.length; i++ ){
								if( monthsRequested[ i ].month == requestMonth && !monthsRequested[ i ].served ){

									// Ensure the UI is only updated once. I noticed the response was handled more than once.
									monthsRequested[ i ].served = true;

									updateUI( data );
									cb( 'updateUI', data );
								}
							}
						} else {
							// No dates.
							scope.selectedDate = null;
						}

						fetching = false;
					})

				}

				/**
				 * Updates the Calendar UI with new data object pass
				 * @param data {object}
				 */
				function updateUI( data ){
					/*
					 * Structure of loadedDates.
					 * var loadedDates = {
					 * year2013:{
					 * 		july:{ ... }
					 * 		 }
					 * }
					 */

					// Store a reference to the loaded dates. This will allow us to cache the dates to avoid service requests.
					var i
						, day
						, month
						, year
						, monthsObj

					scope.selectedDate = null;
					for( i = 0; i < data.D.length; i++ ){
						year = data.D[ i ].substring( 0, 4 );
						month = Number( data.D[ i ].substring( 5, 7 ) );
						day = data.D[ i ].substring( 8, 10 );

						// Initialize the main object.
						loadedDates = loadedDates || {};

						// Initialize the year object.
						loadedDates[ 'year' + year ] = loadedDates[ 'year' + year ] || {};

						// Initialize the month object.
						loadedDates[ 'year' + year ][ months[ month - 1 ] ] = loadedDates[ 'year' + year ][ months[month - 1 ] ] || {};

						// Get a reference to the month object in loadedDates.
						monthsObj = loadedDates[ 'year' + year ][ months[ month - 1 ] ];

						// Store the date as a string right from the service.
						monthsObj[ data.D[ i ] ] = {};

						// Store a reference to the requested month.
						monthsObj[ data.D[ i ] ].date = data.D[ i ];

						// Store a reference to the requested month.
						monthsObj[ data.D[ i ] ].requestMonth = requestMonth;

						// Store the date as an object as well.
						monthsObj[ data.D[ i ] ].dateObj = new Date( year, month - 1, day );
						if( !scope.selectedDate )
							scope.selectedDate = new Date( year, month - 1, day );
					}

					scope.currentYear = year
					scope.currentMonth = months[ month - 1 ]

					cb( 'calendarDataUpdated', loadedDates );

					// Select the first available date and time.
					selectFirstAvailableDateTime( monthsObj );
				}

				function selectFirstAvailableDateTime( monthsObj ){

					var firstDay = getFirstAvailableDate( monthsObj );
					scope.selectedDate = ( monthsObj[ firstDay ] ) ? monthsObj[ firstDay ].dateObj : null;

				}

				function getFirstAvailableDate( monthsObj ){
					// Iterate through all the days in the month and select the earliest date.
					// Return null if there is no reference.
					if( !monthsObj ){
						return null;
					}

					var day = 99999999999 // Start off with a really high number as we will be getting the lesser.
						, nextDay;

					for( var key in monthsObj ){
						if( !key || key == undefined ){
							continue;
						}

						nextDay = Number( key.split( '-' ).join( '' ) );
						day = ( day < nextDay ) ? day : nextDay;
					}

					day = String( day );
					day = day.substring( 0, 4 ) + '-' + day.substring( 4, 6 ) + '-' + day.substring( 6, 8 );

					return day;
				}

				function gateDateStringByDate( date ){
					var dayStr
						, monthStr
						, currentDateString;

					dayStr = String( date.getDate() );
					dayStr = ( dayStr.length == 1 ) ? String( '0' + dayStr ) : dayStr;

					monthStr = String( date.getMonth() + 1 );
					monthStr = ( monthStr.length == 1 ) ? String( '0' + monthStr ) : monthStr;

					currentDateString = date.getFullYear() + '-' + monthStr + '-' + dayStr;

					return currentDateString;
				}

				function requestDateBySelectedMonth( title ){

					var titleArr = title.toLowerCase().split( ' ' );
					var monthName = titleArr[ 0 ]; // Ex. june
					var year = titleArr[ 1 ]; // Ex. 2013

					var monthIndex = getMonthNumberByName( monthName );

					// Determine the requestMonth value based on the month and year.
					if( monthIndex > -1 ){
						requestMonth = datesPreviouslyLoaded( monthIndex, year );

						if( requestMonth != -1 && requestMonth != -2 && requestMonth >= todaysMonth + 1 ){
							requestDates();
						} else if( requestMonth == -2 ){
							// The selected month was previously loaded.
							// Don't do anything here - but don't remove this condition.
						} else {
							// The selected month is earlier than the current month.
							scope.selectedDate = null;
						}
					}
				}

				function getMonthNumberByName( monthName ){
					for( var i = 0; i < months.length; i++ ){
						if( months[ i ].toLowerCase() == monthName ){
							return i;
						}
					}
					return -1;
				}

				function datesPreviouslyLoaded( monthIndex, year ){
					// Check if the requestedMonth has been previously requested.
					// Ex: Today is 06/19/2013
					// requestedMonth = -1 returns the first available month.
					// Assuming first availbale month is 06/2013, to load July/2013 requestedMonth should be 7; August/2-13 8, etc.
					// Following same assumption, to load 01/2014 requestedMonth should be 13, 02/2014 14, etc.
					// Determine what the month should be and see if it exist in the collection.

					var cy = todaysYear;
					var dm = monthIndex + 1; // The month displayed on the calendar. dts.displayedMonth
					var dy = year; // The year displayed on the calendar. dts.displayedYear
					var cm = todaysMonth + 1;
					var requestedMonth = ( ( dy - cy ) * 12 ) + dm;

					if( !loadedDates ){
						return requestedMonth;
					}

					// If the current year is in loadedDates.
					if( loadedDates[ 'year' + dy ] ){
						// If the display month is in loadedDates[ displayYear ]
						if( loadedDates[ 'year' + dy ][ months[ monthIndex ] ] ){
							// Data was previously loaded for this month.
							selectFirstAvailableDateTime( loadedDates[ 'year' + dy ][ months[ monthIndex ] ] );

							// Count at least one date in the selected year-month.
							var c = 0;
							for( var key in loadedDates[ 'year' + dy ][ months[ monthIndex ] ] ){
								if( loadedDates[ 'year' + dy ][ months[ monthIndex ] ].hasOwnProperty( key ) ){
									c++;
									break;
								}
							}

							return -2;
						} else {
							// If there is not at least one date, show the message.
						}
					}

					return requestedMonth;
				}

				function cb( callback, data ){
					// Executes the callbacks defined in the controller
					// while safely failing if callback is not defined.
					try{
						callbacks[ callback ]( data )
					} catch( e ){
						// Callback not defined in the controller.
					}
				}

				requestDates();
			}
			}
			return dir;
		}])
