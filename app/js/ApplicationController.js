/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'ApplicationController'
	,[
		'$rootScope'
		, '$http'
		, '$scope'
		, '$resource'
		, '$log'
		, 'templateService'
		, function
			(
				$rootScope
				, $http
				, $scope
				, $resource
				, $log
				, templateService
				) {

			// Initializes the template service which add any templates defined in it.
			var ts = templateService;
			
			/**
			 * CALLBACK FUNCTIONS:
			 * Callback for calendar directive.
			 *
			 * getDates = return data of json request
			 * gotDates = return json object
			 * updateUI = return json data
			 * selectedDate = return date String, scope.selectedDate is also public
			 * calendarDataUpdated = return json loadedDates
			 */

			$scope.callbacks =
			{	  getDates: function(data){
				$log.log('ApplicationController >> callbacks > gettingDates', data)

			}
				, gotDates: function(data){
				$log.log('ApplicationController >> callbacks > gotDates', data)
			}
				, updateUI: function(data){
				$log.log('ApplicationController >> callbacks > updateUI', data)
			}
				, calendarDataUpdated: function(data){
				$log.log('ApplicationController >> callbacks > calendarDataUpdated', data)
			}
				, selectedDate: function(data){
				$log.log('ApplicationController >> callbacks > selectedDate', data)
			}

			};

			// Options for calendar dierective.
			$scope.calendarOptions =
			{	  showWeeks: false
				, request_type: 'getCalendar'
				, requestObj:{ message:'hello'}
			};


		}]
)