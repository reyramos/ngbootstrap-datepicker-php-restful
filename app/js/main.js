// Require JS  Config File
require({
			baseUrl: 'js/',
			paths: {
				'ua-parser': '../lib/ua-parser-js/src/ua-parser'
				, 'angular': '../lib/angular/index'
				, 'angular-resource': '../lib/angular-resource/index'
				, 'angular-route': '../lib/angular-route/index'
//				, 'angular-datepicker': 'datepicker'
				, 'angular-datepicker': '../lib/angular-bootstrap/src/datepicker/datepicker'
				, 'angular-position': '../lib/angular-bootstrap/src/position/position'

			},
			shim: {
				'app': {
					'deps': [
						  'angular'
						, 'angular-route'
						, 'angular-resource'
						, 'angular-datepicker'
					]
				},
				'angular-resource': { 'deps': ['angular']},
				'angular-route': { 'deps': ['angular'] },
				'angular-position': { 'deps': ['angular'] },
//				'angular-datepicker': { 'deps': ['angular'] },
				'angular-datepicker': { 'deps': ['angular', 'angular-position'] },
				'routes': { 'deps': [
					'app'
				]},
				'templateService' : { 'deps': ['app']},
				'ApplicationController': {
					'deps': [
						'app'
					]}
				, 'calendarDirective':
				{ 'deps':
					[ 'app'
					]
				}
			}
		},
		[
			'require'
			, 'routes'
			, 'ApplicationController'
			, 'calendarDirective'
			, 'templateService'


		],
		function (require) {
			return require(
				[
					'bootstrap'
				]
			)
		}
);